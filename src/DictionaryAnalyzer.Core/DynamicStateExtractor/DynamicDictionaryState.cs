﻿using DictionaryAnalyzer.Core.Interfaces;
using System.Linq;
using System.Runtime.InteropServices;

namespace DictionaryAnalyzer.Core.DynamicStateExtractor
{
    [StructLayout(LayoutKind.Auto)]
    public struct DynamicDictionaryState : IDictionaryState<DynamicDictionaryEntry>
    {
        internal DynamicDictionaryState(int[] buckets, DynamicDictionaryEntry[] entries, int count, int freeList, int freeCount)
        {
            Buckets = buckets;
            Entries = entries;
            Count = count;
            FreeList = freeList;
            FreeCount = freeCount;
        }

        public int[] Buckets { get; }
        public DynamicDictionaryEntry[] Entries { get; }
        public int Count { get; }
        public int FreeList { get; }
        public int FreeCount { get; }

        IDictionaryEntry[] IDictionaryState.Entries => Entries.Cast<IDictionaryEntry>().ToArray();
    }
}