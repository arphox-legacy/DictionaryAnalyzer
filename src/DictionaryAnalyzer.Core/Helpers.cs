﻿using System;

namespace DictionaryAnalyzer.Core
{
    internal static class Helpers
    {
        internal static bool IsGenericDictionary(object obj)
        {
            var dictionaryType = obj.GetType();
            bool isGenericDictionary = dictionaryType.IsGenericType
                && dictionaryType.GetGenericTypeDefinition() == typeof(System.Collections.Generic.Dictionary<,>);
            return isGenericDictionary;
        }
    }
}