﻿namespace DictionaryAnalyzer.Core.Interfaces
{
    public interface IDictionaryStateEqualityComparer
    {
        bool AreEqual(IDictionaryState x, IDictionaryState y);
    }
}