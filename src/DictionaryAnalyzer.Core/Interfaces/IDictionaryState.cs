﻿namespace DictionaryAnalyzer.Core.Interfaces
{
    public interface IDictionaryState<out TEntry> : IDictionaryState
        where TEntry : IDictionaryEntry
    {
        new TEntry[] Entries { get; }
    }

    public interface IDictionaryState
    {
        IDictionaryEntry[] Entries { get; }
        int[] Buckets { get; }
        int Count { get; }
        int FreeList { get; }
        int FreeCount { get; }
    }
}