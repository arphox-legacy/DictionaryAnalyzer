﻿namespace DictionaryAnalyzer.Core.Interfaces
{
    public interface IDictionaryStateExtractor<TState> : IDictionaryStateExtractor
        where TState: IDictionaryState
    {
        new TState ExtractState();
    }

    public interface IDictionaryStateExtractor
    {
        IDictionaryState ExtractState();
    }
}