﻿namespace DictionaryAnalyzer.Core.Interfaces
{
    public interface IDictionaryEntry<out TKey, out TValue> : IDictionaryEntry
    {
        new TKey Key { get; }
        new TValue Value { get; }
    }

    public interface IDictionaryEntry
    {
        int HashCode { get; }
        int Next { get; }
        object Key { get; }
        object Value { get; }
    }
}