﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace DictionaryAnalyzer.Core.Watcher
{
    public sealed class TaskManager : ITaskManager
    {
        private readonly TimeSpan pollingRate;
        private CancellationTokenSource cancellationTokenSource;
        private bool isRunning = false;
        private bool isStopRequested = false;

        public TaskManager(TimeSpan pollingRate)
        {
            this.pollingRate = pollingRate;
        }

        public void Start(Action tickAction)
        {
            if (isStopRequested)
                WaitUntilStopped();
            else if (isRunning)
                throw new InvalidOperationException("Stop the watcher before restarting it.");

            isRunning = true;

            cancellationTokenSource = new CancellationTokenSource();
            Task.Factory.StartNew(
                () => RepeatWatch(tickAction),
                CancellationToken.None,
                TaskCreationOptions.LongRunning,
                TaskScheduler.Current);
        }

        public void Stop()
        {
            cancellationTokenSource.Cancel();
            isStopRequested = true;
        }

        public void Dispose() => Stop();

        private async Task RepeatWatch(Action tickAction)
        {
            while (!cancellationTokenSource.IsCancellationRequested)
            {
                await Task.Delay(pollingRate);
                tickAction();
            }

            isStopRequested = false;
            isRunning = false;
        }

        private void WaitUntilStopped()
        {
            while(isRunning)
                Thread.Sleep(50);
        }
    }
}