﻿using DictionaryAnalyzer.Core.Comparers;
using DictionaryAnalyzer.Core.Interfaces;
using System;

namespace DictionaryAnalyzer.Core.Watcher
{
    internal static class DictionaryStateComparerFactory
    {
        internal static IDictionaryStateEqualityComparer Create(CompareOptions compareOptions)
        {
            switch (compareOptions)
            {
                case CompareOptions.CountOnly: return DictionaryStateCountEqualityComparer.Instance;
            }

            throw new InvalidOperationException();
        }
    }
}