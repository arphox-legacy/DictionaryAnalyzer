﻿using DictionaryAnalyzer.Core.Interfaces;
using System;

namespace DictionaryAnalyzer.Core.Watcher
{
    public sealed class DictionaryWatcher<TState> : IDisposable
        where TState : IDictionaryState
    {
        private readonly IDictionaryStateExtractor<TState> dictionaryStateExtractor;
        private readonly IDictionaryStateEqualityComparer dictionaryStateComparer;
        private readonly ITaskManager taskManager;
        private TState previousState;

        public event EventHandler<TState> StateChanged;

        public DictionaryWatcher(
            IDictionaryStateExtractor<TState> dictionaryStateExtractor,
            IDictionaryStateEqualityComparer dictionaryStateComparer,
            ITaskManager taskManager)
        {
            this.dictionaryStateExtractor = dictionaryStateExtractor ?? throw new ArgumentNullException(nameof(dictionaryStateExtractor));
            this.dictionaryStateComparer = dictionaryStateComparer ?? throw new ArgumentNullException(nameof(dictionaryStateComparer));
            this.taskManager = taskManager ?? throw new ArgumentNullException(nameof(taskManager));
        }

        public void Start() => taskManager.Start(Tick);
        public void Stop() => taskManager.Stop();

        internal void Tick()
        {
            if (previousState == null)
            {
                previousState = dictionaryStateExtractor.ExtractState();
                return;
            }

            var currentState = dictionaryStateExtractor.ExtractState();
            if (!dictionaryStateComparer.AreEqual(previousState, currentState))
            {
                StateChanged?.Invoke(this, currentState);
            }

            previousState = currentState;
        }

        public void Dispose() => taskManager?.Dispose();
    }
}