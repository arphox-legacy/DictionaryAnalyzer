﻿using DictionaryAnalyzer.Core.Interfaces;

namespace DictionaryAnalyzer.Core.Comparers
{
    public sealed class DictionaryStateCountEqualityComparer : IDictionaryStateEqualityComparer
    {
        public static DictionaryStateCountEqualityComparer Instance = new DictionaryStateCountEqualityComparer();

        private DictionaryStateCountEqualityComparer() { }

        public bool AreEqual(IDictionaryState x, IDictionaryState y)
        {
            if (ReferenceEquals(x, y))
                return true;

            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
                return false;

            return x.Count == y.Count && x.FreeCount == y.FreeCount;
        }
    }
}