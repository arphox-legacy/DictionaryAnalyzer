﻿using DictionaryAnalyzer.Core.Interfaces;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace DictionaryAnalyzer.Core.StronglyTypedStateExtractor
{
    [DebuggerDisplay("{Key} -> {Value}, Next: {Next}, Hash: {HashCode}")]
    [StructLayout(LayoutKind.Auto)]
    public struct StronglyTypedDictionaryEntry<TKey, TValue> : IDictionaryEntry<TKey, TValue>
    {
        public int HashCode { get; }
        public int Next { get; }
        public TKey Key { get; }
        public TValue Value { get; }
        object IDictionaryEntry.Key => Key;
        object IDictionaryEntry.Value => Value;

        public StronglyTypedDictionaryEntry(int hashCode, int next, TKey key, TValue value)
        {
            HashCode = hashCode;
            Next = next;
            Key = key;
            Value = value;
        }
    }
}