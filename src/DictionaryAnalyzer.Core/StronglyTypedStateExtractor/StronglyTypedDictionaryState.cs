﻿using DictionaryAnalyzer.Core.Interfaces;
using System.Linq;
using System.Runtime.InteropServices;

namespace DictionaryAnalyzer.Core.StronglyTypedStateExtractor
{
    [StructLayout(LayoutKind.Auto)]
    public struct StronglyTypedDictionaryState<TKey, TValue> : IDictionaryState<StronglyTypedDictionaryEntry<TKey, TValue>>
    {
        internal StronglyTypedDictionaryState(int[] buckets, StronglyTypedDictionaryEntry<TKey, TValue>[] entries, int count, int freeList, int freeCount)
        {
            Buckets = buckets;
            Entries = entries;
            Count = count;
            FreeList = freeList;
            FreeCount = freeCount;
        }

        public int[] Buckets { get; }
        public StronglyTypedDictionaryEntry<TKey, TValue>[] Entries { get; }
        public int Count { get; }
        public int FreeList { get; }
        public int FreeCount { get; }

        IDictionaryEntry[] IDictionaryState.Entries => Entries.Cast<IDictionaryEntry>().ToArray();
    }
}