﻿using DictionaryAnalyzer.Core.DynamicStateExtractor;
using DictionaryAnalyzer.Core.StronglyTypedStateExtractor;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace DictionaryAnalyzer.ConsoleTester
{
    public static class PerformanceReports
    {
        public static void MeasurePerformances(int itemSize = 1_000_000)
        {
            Console.WriteLine("Normal perf:");
            MeasureDictionaryStateExtractorPerformance(itemSize);

            Console.WriteLine("\nDynamic perf:");
            MeasureDynamicDictionaryStateExtractorPerformance(itemSize);
        }

        public static void MeasureDictionaryStateExtractorPerformance(int itemSize, int repetitionCount = 3)
        {
            Dictionary<string, int> dict = PrepareDictionary(itemSize);
            var extractor = new StronglyTypedDictionaryStateExtractor<string, int>(dict);
            Action action = () => extractor.ExtractState();
            MeasurePerformance(action, 3);
        }

        public static void MeasureDynamicDictionaryStateExtractorPerformance(int itemSize, int repetitionCount = 3)
        {
            Dictionary<string, int> dict = PrepareDictionary(itemSize);
            var extractor = new DynamicDictionaryStateExtractor(dict);
            Action action = () => extractor.ExtractState();
            MeasurePerformance(action, 3);
        }

        private static void MeasurePerformance(Action action, int itemSize)
        {
            var measurer = Measure(action, 3);

            List<TimeSpan> results = new List<TimeSpan>();
            foreach (var result in measurer)
            {
                Console.Write((long)result.TotalMilliseconds + "\t");
                results.Add(result);
            }

            double sum = results.Select(x => x.TotalMilliseconds).Sum();
            Console.WriteLine($"\nAVG:{(int)results.Average(x => x.TotalMilliseconds)}");
        }

        private static Dictionary<string, int> PrepareDictionary(int maxSize)
        {
            Console.Write("Initializing dictionary...");
            Dictionary<string, int> asd = new Dictionary<string, int>();

            for (int i = 0; i < maxSize; i++)
                asd[i.ToString()] = i;

            Console.WriteLine("\tDone.");

            return asd;
        }

        private static IEnumerable<TimeSpan> Measure(Action action, int repetitionCount)
        {
            for (int i = 0; i < repetitionCount; i++)
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                action();
                stopwatch.Stop();
                yield return stopwatch.Elapsed;
            }
        }
    }
}