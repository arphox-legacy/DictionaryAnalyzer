﻿using DictionaryAnalyzer.Core.Watcher;
using NUnit.Framework;
using Mocks = DictionaryAnalyzer.Tests.Utility.Mocks;
using Stubs = DictionaryAnalyzer.Tests.Utility.Stubs;

namespace DictionaryAnalyzer.Tests.UnitTests
{
    /// <summary>
    ///     Unit tests for <see cref="DictionaryWatcher{TState}"/>
    /// </summary>
    [TestFixture]
    public sealed class DictionaryWatcherTests
    {
        [Test]
        public void Start_CallsTaskManagerStart()
        {
            // Arrange
            var taskManager = new Mocks.TaskManager();

            var watcher = new DictionaryWatcher<Stubs.DictionaryState>(
                Stubs.DictionaryStateExtractor.Instance,
                Stubs.DictionaryStateEqualityComparer.AlwaysFalseInstance,
                taskManager);

            // Act
            watcher.Start();

            // Assert
            Assert.That(taskManager.StartCallCounter, Is.EqualTo(1));
            Assert.That(taskManager.StopCallCounter, Is.Zero);
        }

        [Test]
        public void Stop_CallsTaskManagerStop()
        {
            // Arrange
            var taskManager = new Mocks.TaskManager();

            var watcher = new DictionaryWatcher<Stubs.DictionaryState>(
                Stubs.DictionaryStateExtractor.Instance,
                Stubs.DictionaryStateEqualityComparer.AlwaysFalseInstance,
                taskManager);

            // Act
            watcher.Stop();

            // Assert
            Assert.That(taskManager.StopCallCounter, Is.EqualTo(1));
            Assert.That(taskManager.StartCallCounter, Is.Zero);
        }

        [Test]
        public void Tick_OnFirstCall_DoesNotRaiseEvent()
        {
            // Arrange
            var watcher = new DictionaryWatcher<Stubs.DictionaryState>(
                Stubs.DictionaryStateExtractor.Instance,
                Stubs.DictionaryStateEqualityComparer.AlwaysFalseInstance,
                Stubs.TaskManager.Instance);

            bool stateChangedRaised = false;
            watcher.StateChanged += (sender, args) => { stateChangedRaised = true; };

            // Act
            watcher.Tick();

            // Assert
            Assert.IsFalse(stateChangedRaised, $"StateChanged event has been raised but it shouldn't have.");
        }

        [Test]
        public void Tick_IfStateChanged_RaisesChangedEvent()
        {
            // Arrange
            var watcher = new DictionaryWatcher<Stubs.DictionaryState>(
                Stubs.DictionaryStateExtractor.Instance,
                Stubs.DictionaryStateEqualityComparer.AlwaysFalseInstance,
                Stubs.TaskManager.Instance);

            bool stateChangedRaised = false;
            watcher.StateChanged += (sender, args) => { stateChangedRaised = true; };

            // Act & Assert
            watcher.Tick();
            Assert.IsFalse(stateChangedRaised, $"StateChanged event has been raised but it shouldn't have.");
            watcher.Tick();
            Assert.IsTrue(stateChangedRaised, $"StateChanged event has NOT been raised but it should have.");
        }

        [Test]
        public void Tick_IfStateNotChanged_DoesNotRaiseChangedEvent()
        {
            // Arrange
            var watcher = new DictionaryWatcher<Stubs.DictionaryState>(
                Stubs.DictionaryStateExtractor.Instance,
                Stubs.DictionaryStateEqualityComparer.AlwaysTrueInstance,
                Stubs.TaskManager.Instance);

            bool stateChangedRaised = false;
            watcher.StateChanged += (sender, args) => { stateChangedRaised = true; };

            // Act & Assert
            watcher.Tick();
            Assert.IsFalse(stateChangedRaised, $"StateChanged event has been raised but it shouldn't have.");
            watcher.Tick();
            Assert.IsFalse(stateChangedRaised, $"StateChanged event has been raised but it shouldn't have.");
        }
    }
}