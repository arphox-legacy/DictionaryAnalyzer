﻿using DictionaryAnalyzer.Core.Comparers;
using DictionaryAnalyzer.Core.StronglyTypedStateExtractor;
using DictionaryAnalyzer.Core.Watcher;
using NUnit.Framework;
using System.Collections.Generic;
using Stubs = DictionaryAnalyzer.Tests.Utility.Stubs;

namespace DictionaryAnalyzer.Tests.ComponentTests
{
    /// <summary>
    ///     Component tests for <see cref="DictionaryWatcher{TState}"/>
    /// </summary>
    [TestFixture]
    public sealed class DictionaryWatcher_CountTests
    {
        [Test]
        public void OneEntryAddition_CountTest()
        {
            // Arrange
            var dictionary = new Dictionary<string, int>();
            var extractor = new StronglyTypedDictionaryStateExtractor<string, int>(dictionary);
            var comparer = DictionaryStateCountEqualityComparer.Instance;
            var taskManager = Stubs.TaskManager.Instance;
            var watcher = new DictionaryWatcher<StronglyTypedDictionaryState<string, int>>(extractor, comparer, taskManager);

            StronglyTypedDictionaryState<string, int> currentState = default;
            watcher.StateChanged += (sender, args) => { currentState = args; };

            // Act
            watcher.Tick(); // First Tick never raises event
            Assert.That(currentState, Is.EqualTo(default(StronglyTypedDictionaryState<string, int>)));

            watcher.Tick(); // We did not change anything yet
            Assert.That(currentState, Is.EqualTo(default(StronglyTypedDictionaryState<string, int>)));

            const string key = "one";
            const int value = 1;
            dictionary[key] = value;
            watcher.Tick();

            // Assert
            Assert.That(currentState.Count, Is.EqualTo(1));
            Assert.That(currentState.FreeCount, Is.EqualTo(0));
        }

        [Test]
        public void TwoEntryAddition_CountTest()
        {
            // Arrange
            var dictionary = new Dictionary<string, int>();
            var extractor = new StronglyTypedDictionaryStateExtractor<string, int>(dictionary);
            var comparer = DictionaryStateCountEqualityComparer.Instance;
            var taskManager = Stubs.TaskManager.Instance;
            var watcher = new DictionaryWatcher<StronglyTypedDictionaryState<string, int>>(extractor, comparer, taskManager);

            StronglyTypedDictionaryState<string, int> currentState = default;
            watcher.StateChanged += (sender, args) => { currentState = args; };

            // Act
            watcher.Tick(); // First Tick never raises event
            Assert.That(currentState, Is.EqualTo(default(StronglyTypedDictionaryState<string, int>)));

            watcher.Tick(); // We did not change anything yet
            Assert.That(currentState, Is.EqualTo(default(StronglyTypedDictionaryState<string, int>)));

            const string key1 = "one";
            const string key2 = "two";
            const int value1 = 1;
            const int value2 = 2;
            dictionary[key1] = value1;
            dictionary[key2] = value2;
            watcher.Tick();

            // Assert
            Assert.That(currentState.Count, Is.EqualTo(2));
            Assert.That(currentState.FreeCount, Is.EqualTo(0));
        }

        [Test]
        public void OneEntryAdditionAndDeletion_CountTest()
        {
            // Arrange
            var dictionary = new Dictionary<string, int>();
            var extractor = new StronglyTypedDictionaryStateExtractor<string, int>(dictionary);
            var comparer = DictionaryStateCountEqualityComparer.Instance;
            var taskManager = Stubs.TaskManager.Instance;
            var watcher = new DictionaryWatcher<StronglyTypedDictionaryState<string, int>>(extractor, comparer, taskManager);

            StronglyTypedDictionaryState<string, int> currentState = default;
            watcher.StateChanged += (sender, args) => { currentState = args; };

            // Act
            watcher.Tick(); // First Tick never raises event
            Assert.That(currentState, Is.EqualTo(default(StronglyTypedDictionaryState<string, int>)));

            watcher.Tick(); // We did not change anything yet
            Assert.That(currentState, Is.EqualTo(default(StronglyTypedDictionaryState<string, int>)));

            const string key = "one";
            const int value = 1;
            dictionary[key] = value;
            dictionary.Remove(key);
            watcher.Tick();

            // Assert
            Assert.That(currentState.Count, Is.EqualTo(1));
            Assert.That(currentState.FreeCount, Is.EqualTo(1));
        }

        [Test]
        public void TwoEntryAdditionAndTwoDeletion_CountTest()
        {
            // Arrange
            var dictionary = new Dictionary<string, int>();
            var extractor = new StronglyTypedDictionaryStateExtractor<string, int>(dictionary);
            var comparer = DictionaryStateCountEqualityComparer.Instance;
            var taskManager = Stubs.TaskManager.Instance;
            var watcher = new DictionaryWatcher<StronglyTypedDictionaryState<string, int>>(extractor, comparer, taskManager);

            StronglyTypedDictionaryState<string, int> currentState = default;
            watcher.StateChanged += (sender, args) => { currentState = args; };

            // Act
            watcher.Tick(); // First Tick never raises event
            Assert.That(currentState, Is.EqualTo(default(StronglyTypedDictionaryState<string, int>)));

            watcher.Tick(); // We did not change anything yet
            Assert.That(currentState, Is.EqualTo(default(StronglyTypedDictionaryState<string, int>)));

            const string key1 = "one";
            const string key2 = "two";
            const int value1 = 1;
            const int value2 = 2;
            dictionary[key1] = value1;
            dictionary[key2] = value2;
            dictionary.Remove(key1);
            dictionary.Remove(key2);
            watcher.Tick();

            // Assert
            Assert.That(currentState.Count, Is.EqualTo(2));
            Assert.That(currentState.FreeCount, Is.EqualTo(2));
        }

        [Test]
        public void TwoEntryAdditionAndOneDeletion_CountTest()
        {
            // Arrange
            var dictionary = new Dictionary<string, int>();
            var extractor = new StronglyTypedDictionaryStateExtractor<string, int>(dictionary);
            var comparer = DictionaryStateCountEqualityComparer.Instance;
            var taskManager = Stubs.TaskManager.Instance;
            var watcher = new DictionaryWatcher<StronglyTypedDictionaryState<string, int>>(extractor, comparer, taskManager);

            StronglyTypedDictionaryState<string, int> currentState = default;
            watcher.StateChanged += (sender, args) => { currentState = args; };

            // Act
            watcher.Tick(); // First Tick never raises event
            Assert.That(currentState, Is.EqualTo(default(StronglyTypedDictionaryState<string, int>)));

            watcher.Tick(); // We did not change anything yet
            Assert.That(currentState, Is.EqualTo(default(StronglyTypedDictionaryState<string, int>)));

            const string key1 = "one";
            const string key2 = "two";
            const int value1 = 1;
            const int value2 = 2;
            dictionary[key1] = value1;
            dictionary[key2] = value2;
            dictionary.Remove(key2);
            watcher.Tick();

            // Assert
            Assert.That(currentState.Count, Is.EqualTo(2));
            Assert.That(currentState.FreeCount, Is.EqualTo(1));
        }
    }
}