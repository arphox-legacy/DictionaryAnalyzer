﻿using DictionaryAnalyzer.Core.Comparers;
using DictionaryAnalyzer.Core.StronglyTypedStateExtractor;
using DictionaryAnalyzer.Core.Watcher;
using DictionaryAnalyzer.Tests.Utility;
using NUnit.Framework;
using System.Collections.Generic;
using Stubs = DictionaryAnalyzer.Tests.Utility.Stubs;

namespace DictionaryAnalyzer.Tests.ComponentTests.DictionaryWatcher
{
    [TestFixture]
    public sealed class DictionaryWatcher_EntryTests
    {
        [Test]
        public void OneEntryAddition_EntryTest()
        {
            // Arrange
            var dictionary = new Dictionary<string, int>();
            var extractor = new StronglyTypedDictionaryStateExtractor<string, int>(dictionary);
            var comparer = DictionaryStateCountEqualityComparer.Instance;
            var taskManager = Stubs.TaskManager.Instance;
            var watcher = new DictionaryWatcher<StronglyTypedDictionaryState<string, int>>(extractor, comparer, taskManager);

            StronglyTypedDictionaryState<string, int> currentState = default;
            watcher.StateChanged += (sender, args) => { currentState = args; };

            // Act
            watcher.Tick(); // First Tick never raises event
            Assert.That(currentState, Is.EqualTo(default(StronglyTypedDictionaryState<string, int>)));

            watcher.Tick(); // We did not change anything yet
            Assert.That(currentState, Is.EqualTo(default(StronglyTypedDictionaryState<string, int>)));

            const string key = "one";
            const int value = 1;
            dictionary[key] = value;
            watcher.Tick();

            // Assert
            var entry = currentState.Entries[0];
            Assert.That(entry.HashCode, Is.EqualTo(key.GetHashCodeForDictionary()));
            Assert.That(entry.Key, Is.EqualTo(key));
            Assert.That(entry.Value, Is.EqualTo(value));
            Assert.That(entry.Next, Is.EqualTo(-1));
        }

        [Test]
        public void TwoEntryAddition_EntryTest()
        {
            // Arrange
            var dictionary = new Dictionary<string, int>();
            var extractor = new StronglyTypedDictionaryStateExtractor<string, int>(dictionary);
            var comparer = DictionaryStateCountEqualityComparer.Instance;
            var taskManager = Stubs.TaskManager.Instance;
            var watcher = new DictionaryWatcher<StronglyTypedDictionaryState<string, int>>(extractor, comparer, taskManager);

            StronglyTypedDictionaryState<string, int> currentState = default;
            watcher.StateChanged += (sender, args) => { currentState = args; };

            // Act
            watcher.Tick(); // First Tick never raises event
            Assert.That(currentState, Is.EqualTo(default(StronglyTypedDictionaryState<string, int>)));

            watcher.Tick(); // We did not change anything yet
            Assert.That(currentState, Is.EqualTo(default(StronglyTypedDictionaryState<string, int>)));

            const string key1 = "one";
            const string key2 = "two";
            const int value1 = 1;
            const int value2 = 2;
            dictionary[key1] = value1;
            dictionary[key2] = value2;
            watcher.Tick();

            // Assert
            var entry1 = currentState.Entries[0];
            Assert.That(entry1.Key, Is.EqualTo(key1));
            Assert.That(entry1.Value, Is.EqualTo(value1));
            Assert.That(entry1.HashCode, Is.EqualTo(key1.GetHashCodeForDictionary()));
            Assert.That(entry1.Next, Is.EqualTo(-1));

            var entry2 = currentState.Entries[1];
            Assert.That(entry2.Key, Is.EqualTo(key2));
            Assert.That(entry2.Value, Is.EqualTo(value2));
            Assert.That(entry2.HashCode, Is.EqualTo(key2.GetHashCodeForDictionary()));
            Assert.That(entry2.Next, Is.EqualTo(-1));
        }

        [Test]
        public void TwoEntryAdditionAndOneDeletion_CountTest()
        {
            // Arrange
            var dictionary = new Dictionary<string, int>();
            var extractor = new StronglyTypedDictionaryStateExtractor<string, int>(dictionary);
            var comparer = DictionaryStateCountEqualityComparer.Instance;
            var taskManager = Stubs.TaskManager.Instance;
            var watcher = new DictionaryWatcher<StronglyTypedDictionaryState<string, int>>(extractor, comparer, taskManager);

            StronglyTypedDictionaryState<string, int> currentState = default;
            watcher.StateChanged += (sender, args) => { currentState = args; };

            // Act
            watcher.Tick(); // First Tick never raises event
            Assert.That(currentState, Is.EqualTo(default(StronglyTypedDictionaryState<string, int>)));

            watcher.Tick(); // We did not change anything yet
            Assert.That(currentState, Is.EqualTo(default(StronglyTypedDictionaryState<string, int>)));

            const string key1 = "one";
            const string key2 = "two";
            const int value1 = 1;
            const int value2 = 2;
            dictionary[key1] = value1;
            dictionary[key2] = value2;
            dictionary.Remove(key2);
            watcher.Tick();

            // Assert
            var entry = currentState.Entries[0];
            Assert.That(entry.HashCode, Is.EqualTo(key1.GetHashCodeForDictionary()));
            Assert.That(entry.Key, Is.EqualTo(key1));
            Assert.That(entry.Value, Is.EqualTo(value1));
            Assert.That(entry.Next, Is.EqualTo(-1));
        }
    }
}