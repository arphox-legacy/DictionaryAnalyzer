﻿using DictionaryAnalyzer.Core.Interfaces;
using System.Linq;

namespace DictionaryAnalyzer.Tests.Utility.Stubs
{
    public sealed class DictionaryState : IDictionaryState
    {
        public static DictionaryState Instance = new DictionaryState();

        private DictionaryState() { }

        public int[] Buckets => null;
        public int Count => 0;
        public int FreeList => 0;
        public int FreeCount => 0;

        public IDictionaryEntry[] Entries => Entries.Cast<IDictionaryEntry>().ToArray();
    }
}