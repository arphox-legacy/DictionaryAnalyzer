﻿using DictionaryAnalyzer.Core.Interfaces;

namespace DictionaryAnalyzer.Tests.Utility.Stubs
{
    public sealed class DictionaryStateExtractor : IDictionaryStateExtractor<DictionaryState>
    {
        public static DictionaryStateExtractor Instance = new DictionaryStateExtractor();

        private DictionaryStateExtractor() { }

        public DictionaryState ExtractState() => DictionaryState.Instance;

        IDictionaryState IDictionaryStateExtractor.ExtractState() => DictionaryState.Instance;
    }
}