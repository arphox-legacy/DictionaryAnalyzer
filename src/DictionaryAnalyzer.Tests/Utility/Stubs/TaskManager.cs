﻿using DictionaryAnalyzer.Core.Watcher;
using System;

namespace DictionaryAnalyzer.Tests.Utility.Stubs
{
    public sealed class TaskManager : ITaskManager
    {
        public static TaskManager Instance = new TaskManager();

        private TaskManager() { }

        public void Start(Action tickAction) { }
        public void Stop() { }
        public void Dispose() { }
    }
}