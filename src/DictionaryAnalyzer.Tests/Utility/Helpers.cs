﻿namespace DictionaryAnalyzer.Tests.Utility
{
    internal static class Helpers
    {
        internal static int GetHashCodeForDictionary(this object obj)
        {
            return obj.GetHashCode() & 0x7FFFFFFF;
        }
    }
}