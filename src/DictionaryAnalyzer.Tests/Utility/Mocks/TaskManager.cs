﻿using DictionaryAnalyzer.Core.Watcher;
using System;

namespace DictionaryAnalyzer.Tests.Utility.Mocks
{
    public sealed class TaskManager : ITaskManager
    {
        public int StartCallCounter = 0;
        public int StopCallCounter = 0;

        public void Start(Action tickAction) => StartCallCounter++;
        public void Stop() => StopCallCounter++;
        public void Dispose() { }
    }
}