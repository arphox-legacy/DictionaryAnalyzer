﻿using DictionaryAnalyzer.Core.Interfaces;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace DictionaryAnalyzer.WpfVisualizer.Views
{
    public partial class BucketsLinkedListView : UserControl
    {
        private const int bucketHeight = 26; // height of bucket rectangles
        private const int fontSizeReducer = 12; // this is subtracted from bucketHeight to get font size inside buckets
        private const int verticalSpaceBetweenBuckets = 4; // vertical space between bucket rectangles

        private int bucketRowCounter = 0;

        public BucketsLinkedListView() => InitializeComponent();

        public void UpdateState(IDictionaryState state)
        {
            insideGrid.RowDefinitions.Clear();
            insideGrid.Children.Clear();
            bucketRowCounter = 0;

            AddModHeader();
            DrawBuckets(state);
        }

        private void DrawBuckets(IDictionaryState state)
        {
            int[] buckets = state.Buckets;
            if (buckets == null)
                return;

            int bucketSize = buckets.Length;
            for (int i = 0; i < bucketSize; i++)
            {
                if (buckets[i] == -1) // empty bucket
                    continue;

                bucketRowCounter++;
                DrawBucket(i, state);
            }
        }

        private void DrawBucket(int mod, IDictionaryState state)
        {
            insideGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(bucketHeight) });
            insideGrid.Children.Add(CreateModValueElement(mod, bucketRowCounter));

            StackPanel bucketsStackPanel = CreateBucketsStackPanel();
            insideGrid.Children.Add(bucketsStackPanel);

            DrawBucketEntries(bucketsStackPanel, state.Entries, state.Buckets[mod]);
        }

        private StackPanel CreateBucketsStackPanel()
        {
            StackPanel stackPanel = new StackPanel
            {
                Orientation = Orientation.Horizontal
            };

            Grid.SetColumn(stackPanel, 1);
            Grid.SetRow(stackPanel, bucketRowCounter);

            return stackPanel;
        }

        /// <summary>
        ///     Adds bucket entries ([key -> value]) to the parent StackPanel
        /// </summary>
        private static void DrawBucketEntries(StackPanel parent, IDictionaryEntry[] entries, int firstEntryIndex)
        {
            IDictionaryEntry entry = entries[firstEntryIndex];

            // Create first entry (it has a different Margin)
            var entryBox = CreateEntryBox(entry);
            parent.Children.Add(entryBox);
            entryBox.Margin = new Thickness(10, 0, 0, 0);

            // Create remaining entries (iterate over the links)
            while (entry.Next != -1)
            {
                parent.Children.Add(CreateRightArrow((int)(bucketHeight * 0.7), (int)(bucketHeight * 0.9))); // ->
                entry = entries[entry.Next];
                parent.Children.Add(CreateEntryBox(entry)); // [key -> value]
            }
        }

        private static UIElement CreateRightArrow(int canvasHeight = 20, int canvasWidth = 25)
        {
            Canvas arrowCanvas = new Canvas()
            {
                Height = canvasHeight,
                Width = canvasWidth
            };

            int endpointX = canvasWidth - 2;
            int endpointY = canvasHeight / 2;

            var lineBody = new Line
            {
                X1 = 1,
                Y1 = canvasHeight / 2,
                X2 = endpointX,
                Y2 = endpointY,
                Stroke = Brushes.Black,
                StrokeThickness = 1
            };

            var lineHeadTopPart = new Line()
            {
                X1 = canvasWidth / 2,
                Y1 = 3,
                X2 = endpointX,
                Y2 = endpointY,
                Stroke = Brushes.Black,
                StrokeThickness = 1
            };

            var lineHeadBottomPart = new Line()
            {
                X1 = canvasWidth / 2,
                Y1 = canvasHeight - 3,
                X2 = endpointX,
                Y2 = endpointY,
                Stroke = Brushes.Black,
                StrokeThickness = 1
            };

            arrowCanvas.Children.Add(lineBody);
            arrowCanvas.Children.Add(lineHeadTopPart);
            arrowCanvas.Children.Add(lineHeadBottomPart);

            return arrowCanvas;
        }

        private static FrameworkElement CreateEntryBox(IDictionaryEntry entry)
        {
            var border = new Border
            {
                Child = new TextBlock
                {
                    Text = $"{entry.Key} -> {entry.Value}",
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(5, 0, 5, 0),
                    FontSize = bucketHeight - fontSizeReducer,
                },

                BorderBrush = Brushes.Black,
                BorderThickness = new Thickness(1),
                Margin = new Thickness(0, 0, 0, 0),
                Height = bucketHeight - verticalSpaceBetweenBuckets,
            };

            return border;
        }

        private static Border CreateModValueElement(int mod, int row)
        {
            var border = new Border
            {
                Child = new TextBlock
                {
                    Text = mod.ToString(),
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(5, 0, 5, 0),
                    FontSize = bucketHeight - fontSizeReducer,
                },
                BorderBrush = Brushes.Black,
                BorderThickness = new Thickness(1),
                Height = bucketHeight - verticalSpaceBetweenBuckets
            };

            Grid.SetColumn(border, 0);
            Grid.SetRow(border, row);

            return border;
        }

        private void AddModHeader()
        {
            var modHeader = new TextBlock
            {
                Text = "mod",
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Margin = new Thickness(5, 5, 5, 5),
                FontSize = bucketHeight - fontSizeReducer,
            };

            Grid.SetColumn(modHeader, 0);
            Grid.SetRow(modHeader, 0);

            insideGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(bucketHeight) });
            insideGrid.Children.Add(modHeader);
        }
    }
}